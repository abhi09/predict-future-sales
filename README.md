# PREDICT FUTURE SALES

https://www.kaggle.com/c/competitive-data-science-predict-future-sales

This challenge serves as final project for the "How to win a data science competition" Coursera course.

In this competition you will work with a challenging time-series dataset consisting of daily sales data, kindly provided by one of the largest Russian software firms - 1C Company. 

We are asking you to predict total sales for every product and store in the next month. By solving this competition you will be able to apply and enhance your data science skills.

# APPROACH

1. XGBOOST
    * Applied Box-plot to figure outliers
    * Introduced Lag features to take into account the timeseries nature of data.
    * Converted time series data to features to feed in XGBOOST
    * Experimented with different possible ensemble methods to improve performance of XGBOOST
2. Time Series Analysis
    * Used ADF( Augmented Dicky Fuller Test) to check stationarity of data
    * Explored AR, MA, and ARMA models - find the process of the data using Autocorrelation Function and Partial Autocorrelation Function
    * Applied Prophet on Hierachichal Time Series Data
    * Implemented Bottom-up strategy to predict the time series
    * Tried to explore work done in Hierarchical TimeSeries (scikit-hts)